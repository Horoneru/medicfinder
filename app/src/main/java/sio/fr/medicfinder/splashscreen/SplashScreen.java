package sio.fr.medicfinder.splashscreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;

import sio.fr.medicfinder.Gps;
import sio.fr.medicfinder.MainActivity;
import sio.fr.medicfinder.R;
import sio.fr.medicfinder.tracker.GpsTracker;

/**
 * Created by Julien on 23/12/2015.
 */

/**
 * L'activité SplashScreen est utilisée pour faire attendre l'utilisateur le temps que l'application
 * la localise et que les vérifications soient faites.
 */
public class SplashScreen extends Activity implements LocationListener{

    private final int SETTINGSREQUESTCODE = 10;
    private final int SPLASH_TIME = 1000;
    private final Runnable GPSINIT = new Runnable(){
        public void run() {
            initGpsTracker();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!isNetworkAvailable()) {
            showNoInternetAlert();
            return;
        }
        new Handler().postDelayed(GPSINIT, SPLASH_TIME);
    }

    /**
     * initGpsTracker est appelée une seconde après l'apparition du SplashScreen
     * Elle permet d'initialiser le GPSTracker en lui ajoutant les données dont il a besoin
     * et de commencer à essayer de recueillir les données de localisation.
     */
    private void initGpsTracker() {
        final GpsTracker gpsTracker = Gps.getInstance();
        gpsTracker.setContext(getApplicationContext());
        gpsTracker.setLocationListener(this);
        gpsTracker.getLocation();
        if (!Gps.getInstance().canGetLocation()) //Impossible de continuer
            showNoGpsAlert();
    }

    @Override
    public void onLocationChanged(Location location) {
        Gps.getInstance().stopUsingGPS(); //On a ce qu'on veut !
        Log.d("Location changed", location.toString()); //Debug
        Gps.getInstance().setLocation(location);
        startMainActivity();
        finish();

    }

    private void startMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}


    private void showNoGpsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("GPS non activé !");

        alertDialog.setMessage("Le GPS n'est pas activé. " +
                "L'application a besoin du GPS pour fonctionner." +
                " Voulez-vous l'activer dans les paramètres GPS ?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Paramètres GPS", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, SETTINGSREQUESTCODE);
            }
        });

        alertDialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.show();
    }

    private void showNoInternetAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Pas d'internet ! ");

        alertDialog.setMessage("Aucune connexion n'est active. " +
                "L'application a besoin d'une connexion internet pour fonctionner." +
                " Veuillez activer une connexion wi-fi ou mobile afin de continuer.");

        alertDialog.setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.setPositiveButton("Réessayer", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(!isNetworkAvailable())
                    showNoInternetAlert();
                else
                    new Handler().postDelayed(GPSINIT, SPLASH_TIME);
            }
        });

        alertDialog.show();

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == SETTINGSREQUESTCODE && resultCode == 0){
            String provider =
                    Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.LOCATION_PROVIDERS_ALLOWED);//Déprécié en API 19, mais aucune alternative (?)
            if(provider != null && !provider.isEmpty()) { //Si le GPS a été activé par le user
                initGpsTracker();
            }
            else {
                finish(); //Impossible de continuer
            }
        }
    }
}